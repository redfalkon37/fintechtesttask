﻿using Spire.Xls;
using System.Diagnostics;

namespace FinTech.Controllers
{
    internal class ExcelExporter
    {
        private Workbook excelWorkbook;

        /// <summary>
        /// Создание книги эксель
        /// </summary>
        /// <returns></returns>
        public Workbook CreateExcelFile()
        {
            excelWorkbook = new Workbook();
            excelWorkbook.Worksheets.Clear();
            excelWorkbook.Worksheets.Add("Report");

            return excelWorkbook;
        }

        /// <summary>
        /// Запуск книги эксель
        /// </summary>
        /// <param name="workbook"></param>
        public void Start(Workbook workbook)
        {
            workbook.SaveToFile("Report.xlsx", ExcelVersion.Version2010);

            var process = new Process();
            process.StartInfo = new ProcessStartInfo(workbook.FileName)
            {
                UseShellExecute = true,
            };
            process.Start();
        }
    }
}

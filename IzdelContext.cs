﻿using FinTech.Models;
using Microsoft.EntityFrameworkCore;

namespace FinTech
{
    internal class IzdelContext : DbContext
    {
        public DbSet<Izdel> Izdels { get; set; }
        public DbSet<Links> IzdelLinks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlite("Data Source=izdels.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Izdel>().HasData(
                new Izdel { Id = 1, Name = "Дрель", Price = 1000 },
                new Izdel { Id = 2, Name = "Ручка", Price = 300 },
                new Izdel { Id = 3, Name = "Рукоятка", Price = 250 },
                new Izdel { Id = 4, Name = "Корпус", Price = 500 },
                new Izdel { Id = 5, Name = "Ступор", Price = 150 },
                new Izdel { Id = 6, Name = "Кожух", Price = 500 },
                new Izdel { Id = 7, Name = "Молоток", Price = 400 },
                new Izdel { Id = 8, Name = "Набалдашник", Price = 900 });

            modelBuilder.Entity<Links>().HasData(
                new Links { Id = 1, IzdelUp = 0, Izdel = 1, Kol = 1 },
                new Links { Id = 2, IzdelUp = 1, Izdel = 2, Kol = 10 },
                new Links { Id = 3, IzdelUp = 1, Izdel = 3, Kol = 2 },
                new Links { Id = 4, IzdelUp = 3, Izdel = 5, Kol = 2 },
                new Links { Id = 5, IzdelUp = 1, Izdel = 4, Kol = 1 },
                new Links { Id = 6, IzdelUp = 4, Izdel = 2, Kol = 1 },
                new Links { Id = 7, IzdelUp = 4, Izdel = 6, Kol = 5 },
                new Links { Id = 8, IzdelUp = 0, Izdel = 7, Kol = 1 },
                new Links { Id = 9, IzdelUp = 7, Izdel = 8, Kol = 20 },
                new Links { Id = 10, IzdelUp = 7, Izdel = 3, Kol = 10 });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinTech.Models
{
    internal class Links
    {
        public long Id { get; set; }
        public long IzdelUp { get; set; }
        public long Izdel { get; set; }
        public int Kol { get; set; }
    }
}

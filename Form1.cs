﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using Spire.Xls;
using FinTech.Controllers;

namespace FinTech
{
    /// <summary>
    /// Расчет стоимости товара и экспорт данных в эксель
    /// </summary>
    public partial class ExcelExport : Form
    {
        private ExcelExporter excelExporter;

        private Workbook workbook;
        private Worksheet sheet;

        private IzdelContext? dbContext;

        private int hierarchyNumber = 0;

        private string[] tableTitle = new string[4]
        {
            "Изделие", "Количество", "Стоимость", "Цена"
        };

        private const int FIRST_IN_HIERARCHY = 0;

        public ExcelExport()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.dbContext = new IzdelContext();
            this.dbContext.Database.EnsureDeleted();
            this.dbContext.Database.EnsureCreated();
            this.dbContext.Izdels.Load();
            this.dbContext.IzdelLinks.Load();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            this.dbContext?.Dispose();
            this.dbContext = null;
        }

        private void ReportButton_Click(object sender, EventArgs e)
        {
            GenerateExcelFile(); //Создаем рабочую книгу
            DrawTable(); //Рисуем таблицу

            var parentList = dbContext.IzdelLinks.Where(i => i.IzdelUp == 0);
            FillData(parentList, out decimal cost); // Запускаем цикл расчета

            sheet.Range[1, 1, sheet.LastDataRow, sheet.LastDataColumn].BorderInside();
            sheet.Range[1, 1, sheet.LastDataRow, sheet.LastDataColumn].BorderAround();

            excelExporter.Start(workbook); // Запуск документа
        }

        private void FillData(IQueryable<Models.Links> list, out decimal parentCost)
        {
            parentCost = 0;

            foreach (var link in list)
            {
                int row = sheet.LastDataRow + 1;

                var nameCell = sheet.Range[row, 1];
                var countCell = sheet.Range[row, 2];
                var costCell = sheet.Range[row, 3];
                var priceCell = sheet.Range[row, 4];

                if (link.IzdelUp == FIRST_IN_HIERARCHY)
                {
                    parentCost = 0;
                }

                var izdel = dbContext.Izdels.Find(link.Izdel);
                string text = izdel != null ? izdel.Name : " ";

                for (int i = 0; i < hierarchyNumber; i++)
                {
                    text = $"\t\t{text}";
                }

                nameCell.Text = text;
                countCell.Text = link.Kol.ToString();
                priceCell.Text = izdel != null ? izdel.Price.ToString() : "0";

                decimal cost = izdel != null ? link.Kol * izdel.Price : 0;

                CellRange[] cells = new CellRange[] { nameCell, priceCell };
                CellConfig(cells); // Выравнивание

                var childList = dbContext.IzdelLinks.Where(i => i.IzdelUp == link.Izdel);

                if (childList.Count() != 0)
                {
                    hierarchyNumber++;

                    FillData(childList, out decimal childCost);

                    parentCost = parentCost + childCost + cost;
                    costCell.Text = link.IzdelUp == 0 ? parentCost.ToString() : (childCost + cost).ToString();
                }
                else
                {
                    parentCost += cost;
                    costCell.Text = cost.ToString();
                }
            }
            hierarchyNumber--;
        }

        private void CellConfig(CellRange[] cells)
        {
            foreach (var cell in cells)
            {
                cell.ColumnWidth = cell.Text.Length * 1.5;
            }
        }

        private void DrawTable()
        {
            CellRange[] cells = new CellRange[tableTitle.Length];

            for (int i = 0; i < tableTitle.Length; i++)
            {
                var cell = sheet.Range[1, sheet.LastDataColumn + 1];
                cell.Text = tableTitle[i];
                cells[i] = cell;
            }

            CellConfig(cells);
        }

        private void GenerateExcelFile()
        {
            excelExporter = new ExcelExporter();
            workbook = excelExporter.CreateExcelFile();
            sheet = workbook.Worksheets["Report"];
        }
    }
}
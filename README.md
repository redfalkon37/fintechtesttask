# FinTechTestTask



## Getting started

## Name
FinTech Test Task

## Description
Тестовое задание для компании Финансовые Технологии.

## Installation
В данном репозитории присутствуют только файлы проекта.

## Usage
Использовалась библиотека Spire.XLS, для взаимодействия с ексель, и SQLite EF Code First для базы данных.
